package com.techpoa.springbootstellarbank.service;

import com.techpoa.springbootstellarbank.dto.UserRequest;
import com.techpoa.springbootstellarbank.entity.Status;
import com.techpoa.springbootstellarbank.entity.User;
import com.techpoa.springbootstellarbank.repository.UserRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public List<User> getUsers() {
        log.info("Fetching all users from database ");
        return userRepository.findAll();
    }

    @Override
    public User saveUser(UserRequest userRequest) {

        log.info("Saving new user to the database {} ", userRequest.email());
        User user = User.builder().email(userRequest.email()).status(Status.ACCOUNT_CREATED).build();
        return userRepository.save(user);

    }

    @Override
    public Optional<User> findUserByEmail(String email) {

        log.info("Fetching user by email {} ", email);
        return Optional.of(userRepository.findByEmail(email));
    }
}

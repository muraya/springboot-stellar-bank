package com.techpoa.springbootstellarbank.service;


import com.techpoa.springbootstellarbank.configuration.ApplicationConfiguration;
import com.techpoa.springbootstellarbank.dto.WalletRequest;
import com.techpoa.springbootstellarbank.entity.Status;
import com.techpoa.springbootstellarbank.entity.User;
import com.techpoa.springbootstellarbank.entity.Wallet;
import com.techpoa.springbootstellarbank.repository.WalletRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.stellar.sdk.*;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Service
@Slf4j
public class WalletServiceImpl implements WalletService {

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    ApplicationConfiguration configuration;

    public List<Wallet> getWallets(){
        log.info("Fetching all wallets from database ");
        return walletRepository.findAll();
    }

    public Wallet saveWallet(WalletRequest walletRequest){
        log.info("Saving new wallet to the database with name {} ", walletRequest.accountName());

        Wallet wallet = Wallet.builder()
                .publicKey(walletRequest.publicKey())
                .privateKey(walletRequest.privateKey())
                .accountName(walletRequest.accountName())
                .build();

        return walletRepository.save(wallet);
    }

    public Optional<Wallet> findUserWallet(long id){
        return walletRepository.findById(id);
    }

    public String openAccount(String email) {

        String key = null;
        InputStream response = null;
        try {

            KeyPair pair = KeyPair.random();
            String seed = new String(pair.getSecretSeed());
            key = pair.getAccountId();

            String friendBotUrl = String.format(configuration.getStellarNetworkFriendlyBot(), key);

            response = new URL(friendBotUrl).openStream();
            String body = new Scanner(response, "UTF-8").useDelimiter("\\A").next();

            log.info("New Stellar account created :: {} " , body);

            Wallet wallet =  Wallet.builder().publicKey(key).privateKey(seed).build();
            walletRepository.save(wallet);
            response.close();
        } catch (IOException e) {
            log.error(" {} error when opening account : message is {} " , e.getClass().getSimpleName());
        }

        return key;
    }

    public boolean transferFunds(String from, String to, String amount, String memo) {

        log.info("Funda transfer from {} to {} amounting to {} with memo {}", from, to, amount, memo);

        Network.useTestNetwork();
        Server server = new Server(configuration.getStellarNetworkUrl());
        KeyPair source = KeyPair.fromSecretSeed(from);
        KeyPair destination = KeyPair.fromAccountId(to);
        AccountResponse sourceAccount = null;
        try {
            sourceAccount = server.accounts().account(source);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        // Start building the transaction.
        Transaction transaction = new Transaction.Builder(sourceAccount)
                .addOperation(new PaymentOperation.Builder(destination, new AssetTypeNative(), amount).build())
                // A memo allows you to add your own metadata to a transaction.
                // It's
                // optional and does not affect how Stellar treats the
                // transaction.
                .addMemo(Memo.text(memo)).build();
        // Sign the transaction to prove you are actually the person sending it.
        transaction.sign(source);

        // And finally, send it off to Stellar!
        try {

            SubmitTransactionResponse response = server.submitTransaction(transaction);

            log.info("Transaction submitted successfully with response {} {}", response.isSuccess(), response.getResultXdr());

        } catch (Exception e) {

            log.error("{} error while initiating transfer with message {}", e.getClass().getSimpleName(), e.getMessage());
            // If the result is unknown (no response body, timeout etc.) we
            // simply resubmit
            // already built transaction:
            // SubmitTransactionResponse response =
            // server.submitTransaction(transaction);
        }

        return true;
    }

    public Wallet findWalletByUser(User user){

        Wallet wallet = Wallet.builder().build();
        return wallet;
    }

    public void saveWallet(Wallet wallet){

    }


}

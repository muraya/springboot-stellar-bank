package com.techpoa.springbootstellarbank.service;

import com.techpoa.springbootstellarbank.dto.UserRequest;
import com.techpoa.springbootstellarbank.dto.WalletRequest;
import com.techpoa.springbootstellarbank.entity.User;
import com.techpoa.springbootstellarbank.entity.Wallet;

import java.util.List;
import java.util.Optional;

public interface WalletService {

    public List<Wallet> getWallets();

    public Wallet saveWallet(WalletRequest walletRequest);

    public Optional<Wallet> findUserWallet(long id);

//    String openAccount(String email);
//    float getStellarNetworkBalance(String accountKey);
//
//    boolean transferFunds(String from, String to, String amount, String memo);
//    public Wallet findWalletByUser(User user);
//    public void saveWallet(Wallet wallet);
//
//
//
}
//
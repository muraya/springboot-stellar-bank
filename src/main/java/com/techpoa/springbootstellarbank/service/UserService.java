package com.techpoa.springbootstellarbank.service;

import com.techpoa.springbootstellarbank.dto.UserRequest;
import com.techpoa.springbootstellarbank.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    public List<User> getUsers();

    public User saveUser(UserRequest user);

    public Optional<User> findUserByEmail(String email);

}

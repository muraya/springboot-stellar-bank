package com.techpoa.springbootstellarbank.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public record WalletRequest(String privateKey, String publicKey, String accountName) {
}

package com.techpoa.springbootstellarbank.dto;


import lombok.*;

@AllArgsConstructor
@Data
@Builder
public record UserRequest(String email) {

}

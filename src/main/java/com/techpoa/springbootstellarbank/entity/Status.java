package com.techpoa.springbootstellarbank.entity;

public enum Status {
    ACCOUNT_CREATED, WALLET_CREATED;
}

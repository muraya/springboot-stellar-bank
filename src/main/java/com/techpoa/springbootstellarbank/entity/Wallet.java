package com.techpoa.springbootstellarbank.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "wallet")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "public_key")
    private String publicKey;

    @Column(name = "private_key")
    private String privateKey;

    //this also matches email of the user
    @Column(name = "account_name",unique=true)
    private String accountName;

    @Column(name = "balance")
    private float balance;

    @ManyToOne
    @PrimaryKeyJoinColumn
    private User user;

}

package com.techpoa.springbootstellarbank.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private int user_id;

    @Column(name = "email",unique=true)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", columnDefinition = "default Status.ACCOUNT_CREATED")
    private Status status;

    @Column(name = "created_at")
    private Date created_at;


}

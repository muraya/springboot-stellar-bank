package com.techpoa.springbootstellarbank.repository;

import com.techpoa.springbootstellarbank.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

}

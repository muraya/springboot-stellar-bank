package com.techpoa.springbootstellarbank.repository;

import com.techpoa.springbootstellarbank.entity.User;
import com.techpoa.springbootstellarbank.entity.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletRepository extends JpaRepository<Wallet, Long> {
    Wallet findByUser(User user);
}

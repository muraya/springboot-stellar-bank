package com.techpoa.springbootstellarbank.controller;

import com.techpoa.springbootstellarbank.dto.UserRequest;
import com.techpoa.springbootstellarbank.dto.WalletRequest;
import com.techpoa.springbootstellarbank.entity.User;
import com.techpoa.springbootstellarbank.entity.Wallet;
import com.techpoa.springbootstellarbank.service.UserService;
import com.techpoa.springbootstellarbank.service.WalletService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
public class WalletController {

    private final WalletService walletService;

    // list all wallets

    // findUserWallet

    // fundUserWallet

    @GetMapping("/users")
    public ResponseEntity<List<Wallet>> getAllUsers() {

        return ResponseEntity.ok().body(walletService.getWallets());
    }
    @PostMapping("/users")
    public ResponseEntity<Wallet> createUser(@RequestBody WalletRequest walletRequest)
    {
        return ResponseEntity.ok().body(walletService.saveWallet(walletRequest));
    }
}

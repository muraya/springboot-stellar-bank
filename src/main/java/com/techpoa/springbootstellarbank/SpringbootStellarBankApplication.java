package com.techpoa.springbootstellarbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootStellarBankApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootStellarBankApplication.class, args);
    }


}

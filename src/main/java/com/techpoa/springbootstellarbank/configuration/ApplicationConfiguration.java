package com.techpoa.springbootstellarbank.configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class ApplicationConfiguration {

    @Value("${stellar.network.url}")
    String stellarNetworkUrl;

    @Value("${stellar.network.friendbot}")
    String stellarNetworkFriendlyBot;

    @Value("${app.http.connection.timeout}")
    int httpConnectionTimeout;

    @Value("${app.http.read.timeout}")
    int httpReadTimeout;

}

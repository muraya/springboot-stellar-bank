package com.techpoa.springbootstellarbank.exception;

public class HttpException extends GeneralException {

    public HttpException(String msg) {
        super(msg);
    }

}
